# ReduxSimpleStarter

### Getting Started
Checkout this repo, install dependencies, then start the gulp process with the following:

```
> git clone git@bitbucket.org:adearlife/react_redux_starter.git
> cd ReduxSimpleStarter
> npm install
> npm start
```